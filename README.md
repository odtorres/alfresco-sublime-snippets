# Alfresco-Sublime-Snippets

[![Join the chat at https://gitter.im/odtorres/Alfresco-Sublime-Snippets](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/odtorres/Alfresco-Sublime-Snippets?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
>A collection of sublime text snippets useful for coding Alfresco webscripts, scripts, forms and content models.

## Usage
![screencast](http://i.imgur.com/2fPGHUV.gif)

## Installation

 Download the [.zip](https://bitbucket.org/odtorres/alfresco-sublime-snippets/get/master.zip) file and unzip it into your Sublime Text packages directory.

 * Linux: /home/yourUser/.config/sublime-text.../Packages/User
 * Windows: C:\Users\yourUser\AppData\Roaming\Sublime Text ...\Packages\User

## Contents
1. Alfresco JavaScript API
2. Description Webscripts xml
3. Freemarker Template
4. Content Model
5. Alfresco web client Forms

## Links
The official site is at <http://odtorres.github.io/Alfresco-Sublime-Snippets/>.

If you need help post your specific question in the [Gitter Chat.](https://gitter.im/odtorres/Alfresco-Sublime-Snippets)

## Author
Bitbucket: [odtorres](https://bitbucket.org/odtorres)

## License
  Alfresco Sublime Text Snippets by Oscar Daniel Torres Hdez <odtorres@uci.cu>

  Copyright (C) 2015 Oscar Daniel Torres Hdez.

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.